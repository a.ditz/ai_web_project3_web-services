# WEB-AI Movie recommender system

This is Group 19's Project 3 **Web service applications**.

## Authors and acknowledgment
Alexander Gerhard Ditz

Nils Niehaus

Leonard von Heiz

## Description

This 'python' project consists of:

- Multiple flask apps
- A hub, a client and two channels (a guessing game chat and a simple eliza chatbot)


## Requirements

You need a 'python' environment with the packages:
- flask
- flask-sqlalchemy
- requests
installed,
as well as an internet connection.

## Usage

To use the applictaion from your local directory, **run 'flask --app client run'** in the terminal. But before that you need to run the hub and the two channels and register the channels via **'flask --app channelname.py register'**.


## Features

The eliza catbot gives appropriate answers to whatever message you sent it.
The guessing game works in the way that the computer choses a random integer number and you have to guess it (message in your guesses). 


## Implementation

Eliza is a simple implementation of a predefined python class that uses doctor.txt as a database. The guessing game simply analyzes your message for a number and if it includes one it compares taht guess with the number it chose.

## Gitlab

https://gitlab.gwdg.de/a.ditz/ai_web_project3_web-services

